import { gsap } from "gsap";


const fadeInLogo = ()=> gsap.from("#logo", 
    {opacity: 0, 
    y: -100, 
    duration: 2});

const fadeInCongratsMsg = ()=> gsap.from(".congrats", 
    {opacity: 0, 
    y: -100, 
    duration: 2});

const fadeInCashMsg = ()=> gsap.from(".cash", 
    {opacity: 0, 
    y: -100, 
    duration: 2});

const fadeInForm = ()=> gsap.from("form", 
    {opacity: 0, 
    y: -100, 
    duration: 2});

const moneyShake = ()=> gsap.from("#moneyImg", 
    0.1, 
    {x:"+=20", 
    yoyo:true, 
    repeat:20});


const testAnimation = () => {
    fadeInLogo()
    fadeInCongratsMsg()
    fadeInCashMsg()
    moneyShake()
    fadeInForm()
    console.log("from animations");
}

export default testAnimation